import { useState } from "react";

const useTodoState = (initialState) => {
    const [todos, setTodos] = useState(initialState || []);
    
    const fillTodos = (todos) => {
        setTodos(todos)
    }

    const removeTodo = (todo) => {
        //setTodos(...)
    }

    const addTodo = (todo) => {
        //setTodos(...)
    }

    const toggleTodo = (todo) => [
        //setTodos(...)
    ]

    return {
        state: todos,
        actions: {
            removeTodo,
            addTodo,
            toggleTodo,
            fillTodos
        }
    }
}

export default useTodoState;