const useFetch = () => {
    const getTodos = async () => {
        return await (await fetch('https://jsonplaceholder.typicode.com/todos')).json()
    }
    
    return {
        getTodos
    }
}

export default useFetch;