import React, { useEffect } from "react";
import List from './components/List'; 
import useFetch from './hooks/useFetch'
import useTodoState from "./hooks/useTodoState";

function App() {
  const {getTodos} = useFetch();
  const {state, actions} = useTodoState([]);

  useEffect(() => {
    (async () => {
      const res = await getTodos();
      actions.fillTodos(res);
    })()
  }, [])

  return (
    <div className="wrapper">
      <div className="inner">
        <h1 className="main_text">React tutorial</h1>
        <List toggleTodo={actions.toggleTodo} removeTodo={actions.removeTodo} todos={state} />
      </div>
    </div>
  );
}

export default App;
