import React from 'react';
import style from "./item.module.scss"

const styles = {
  li:{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    padding: ".6rem 1rem",
    border: "1px solid #000",
    borderRadiys: "4px",
    marginBottom: ".5rem"

  },

  span:{
    
    
  },

  input:{
    marginRight: "1rem",
    marginTop:".1rem"
  },

  strong:{
    marginRight: '.1rem'
  }
}
function item({todo,index}) {
  return (
      <li className={style.item}>
        <span>
          <input style={styles.input} type = 'checkbox'/>
          <strong style={styles.strong}>{index + 1}</strong>{todo.title}
        </span>
        <button className={style.removeBtn}>-</button>
      </li>
  );
}



export default item;