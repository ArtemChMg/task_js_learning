import React from "react";
import propTypes from "prop-types";
import Item from "./item";

const styles = {
  ul: {
    listStyle: "none",
    color: "red",
  },
};

function List(props) {
  return (
    <ul style={styles.ul}>
      {props.todos.map((todo, index) => {
        return <Item onDelete={props.removeTodo} onToggle={props.toggleTodo} todo={todo} index={index} key={todo.id} />;
      })}
    </ul>
  );
}

List.propTypes = {
  todos: propTypes.arrayOf(propTypes.object).isRequired,
};

export default List;
